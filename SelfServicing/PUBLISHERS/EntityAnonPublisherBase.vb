﻿Imports isr.Data.LLBLGen.EntityExtensions
''' <summary>
''' Base class for anonymously publishing entity information to a single observer.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="10/03/2010" by="David Hary" revision="1.0.3928.x">
''' Created
''' </history>
Public MustInherit Class EntityAnonPublisherBase(Of TEntity As {SD.LLBLGen.Pro.ORMSupportClasses.EntityBase, SD.LLBLGen.Pro.ORMSupportClasses.IEntity})
    Inherits EntityPublisherBase
    Implements IEntityAnonPublisher(Of TEntity)

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Constructs the class.
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub New()
        MyBase.New()
        Me._EntityName = ""
    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)

        If Not Me.IsDisposed Then

            Try

                If disposing Then

                    ' remove reference to the entity
                    _entity = Nothing

                End If

                ' Free shared unmanaged resources

            Finally

                MyBase.Dispose(disposing)

            End Try

        End If

    End Sub

#End Region

#Region " HELPERS "

    ''' <summary>
    ''' Builds prefetch path for the entity.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected MustOverride Function BuildPrefetchPath() As SD.LLBLGen.Pro.ORMSupportClasses.IPrefetchPath

#End Region

#Region " ENTITY NAME "

    Private _EntityName As String
    ''' <summary>
    ''' Gets or sets the name of the entity.
    ''' </summary>
    ''' <value>
    ''' The name of the entity.
    ''' </value>
    Public Overrides Property EntityName() As String
        Get
            If _entity Is Nothing Then
                Return _EntityName
            Else
                Return _entity.LLBLGenProEntityName
            End If
        End Get
        Set(ByVal value As String)
            _EntityName = value
        End Set
    End Property

#End Region

#Region " ENTITY NO PUBLISH "

    ''' <summary>
    ''' Saves the active entity.
    ''' </summary>
    ''' <returns>True if entity was save and successfuly refetched.</returns>
    ''' <remarks></remarks>
    Public Overrides Function SaveEntity() As Boolean

        Try

            Dim fieldNames As String = String.Join(",", Me.Entity.GetPrimaryKeyFieldNames.ToArray)
            Me.HandleMessage(TraceEventType.Verbose, "SAVING ENTITY", "Saving {0} '{1}'", Me.Entity, fieldNames)
            If Me.Entity.Save() Then
                ' refetch to keep entity active.
                If Me.Entity.Refetch() Then
                    Return True
                Else
                    Me.HandleMessage(TraceEventType.Warning, "FAILED FETCHING ENTITY AFTER SAVING", _
                                     "Failed fetching '{0}' '{1}' from the database after saving.", Me.Entity, fieldNames)
                    Return False
                End If
            Else
                Me.HandleMessage(TraceEventType.Warning, "FAILED SAVING ENTITY", _
                                 "Failed saving '{0}' '{1}' to the database.", Me.Entity, fieldNames)
                Return False
            End If
            Return False

        Catch ex As SD.LLBLGen.Pro.ORMSupportClasses.ORMQueryExecutionException

            Me.HandleMessage(TraceEventType.Warning, "EXCEPTION OCCURRED SAVING ENTITY", _
                             "Exception occured saving '{1}' to the database.{0}Query Executed: {2}.{0}Details: {3}", _
                             Environment.NewLine, Me.Entity, ex.QueryExecuted, ex)
            Return False

        End Try

    End Function

#End Region

#Region " ENTITY WITH PUBLISH "

    ''' <summary>
    ''' Deletes the selected entity.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overrides Function Delete() As Boolean
        If Me.Entity.Delete() Then
            Return Me.NewEntity()
        End If
        Return True
    End Function

    ''' <summary>
    ''' Sets the entity.
    ''' </summary>
    ''' <param name="value"></param>
    ''' <remarks></remarks>
    Public Sub EntitySetter(ByVal value As TEntity)
        _entity = value
    End Sub

    Private WithEvents _entity As TEntity
    ''' <summary>
    ''' Returns reference to the entity.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Entity() As TEntity Implements IEntityAnonPublisher(Of TEntity).Entity
        Get
            Return _entity
        End Get
    End Property

    ''' <summary>
    ''' Instantiates a new entity. Must be overriden
    ''' </summary><returns></returns>
    Public Overrides Function NewEntity() As Boolean
        OnEntityChanged()
        Return True
    End Function

    ''' <summary>
    ''' Saves the active entity.
    ''' </summary>
    ''' <returns>True if entity was save and successfuly refetched.</returns>
    ''' <remarks></remarks>
    Public Overrides Function Save() As Boolean

        If Me.SaveEntity Then
            Me.Refresh()
            'Me.OnEntitySaved()
            'Me.OnEntityChanged()
            Return True
        End If
        Return False

    End Function

#End Region

#Region " EVENTS "

    ''' <summary>
    ''' Reports a property change.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub _PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Handles _entity.PropertyChanged
        If Me._entity.Fields(e.PropertyName) IsNot Nothing AndAlso Me._entity.Fields(e.PropertyName).IsPrimaryKey Then
            ' if pk changed, handle the change. This could be used to fetch a new entity. 
            Me.OnPrimaryKeyChanged()
        Else
            MyBase.OnPropertyChanged(e)
        End If
    End Sub

#End Region

End Class
