﻿''' <summary>
''' Defines the contract implemented by the entity sync publisher for an entity 
''' having a primary key consisting of one field.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="10/03/2010" by="David Hary" revision="1.0.3928.x">
''' Created
''' </history>
<CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1005:AvoidExcessiveParametersOnGenericTypes")> _
Public Interface IEntitySyncPublisher1(Of TEntity As {SD.LLBLGen.Pro.ORMSupportClasses.EntityBase2, SD.LLBLGen.Pro.ORMSupportClasses.IEntity2}, _
                                     TPrimaryKey)
    Inherits isr.Core.ISyncPublisher, IEntitySyncPublisher(Of TEntity)

    ''' <summary>
    ''' Fetches an entity or clears the exiting entity.
    ''' </summary>
    ''' <param name="primaryKey">Specifies the primary key</param>
    ''' <returns>True if entity exists. Otherwise, a new entity is created.</returns>
    ''' <remarks></remarks>
    Function Fetch(ByVal primaryKey As TPrimaryKey) As Boolean

    ''' <summary>
    ''' Fetches an entity or clears the exiting entity.
    ''' </summary>
    ''' <param name="adapter">Specifies an active <see cref="SD.LLBLGen.Pro.ORMSupportClasses.DataAccessAdapterBase">adapter</see>.</param>
    ''' <param name="primaryKey">Specifies the primary key</param>
    ''' <returns>True if entity exists. Otherwise, a new entity is created.</returns>
    ''' <remarks></remarks>
    Function Fetch(ByVal adapter As SD.LLBLGen.Pro.ORMSupportClasses.DataAccessAdapterBase, ByVal primaryKey As TPrimaryKey) As Boolean

    ''' <summary>
    ''' Looks for an entity.
    ''' </summary>
    ''' <param name="primaryKey">Specifies the primary key</param>
    ''' <returns>True if entity exists.</returns>
    ''' <remarks></remarks>
    Function Find(ByVal primaryKey As TPrimaryKey) As Boolean

End Interface
