﻿''' <summary>
''' Defines the contract implemented by the entity observer.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="10/03/2010" by="David Hary" revision="1.0.3928.x">
''' Created
''' </history>
Public Interface IEntityObserver(Of TEntity As {SD.LLBLGen.Pro.ORMSupportClasses.EntityBase2, SD.LLBLGen.Pro.ORMSupportClasses.IEntity2})

    Inherits System.ComponentModel.ISynchronizeInvoke, isr.Core.IMessageObserver

    ''' <summary>
    ''' Handles the update of the list of entities.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function OnEntitiesChanged() As Boolean

    ''' <summary>
    ''' Handles the selection of a new entity.
    ''' </summary>
    ''' <param name="entity">Specifies the entity or nothing if none changed.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function OnEntityChanged(ByVal entity As TEntity) As Boolean

    ''' <summary>
    ''' Handles a change of an existing entity.
    ''' </summary>
    ''' <param name="entity">Specifies the entity or nothing if none changed.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function OnEntityPropertyChanged(ByVal entity As TEntity) As Boolean

    ''' <summary>
    ''' Handles saving or updating an Entity.
    ''' </summary>
    ''' <param name="entity">Specifies the entity or nothing if none saved.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function OnEntitySaved(ByVal entity As TEntity) As Boolean

End Interface
