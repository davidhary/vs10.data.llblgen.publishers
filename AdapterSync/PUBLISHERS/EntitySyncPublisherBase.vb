﻿Imports isr.Data.LLBLGen.EntityExtensions
''' <summary>
''' Base class for sync publishing entity information for an entity.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="10/03/2010" by="David Hary" revision="1.0.3928.x">
''' Created
''' </history>
Public MustInherit Class EntitySyncPublisherBase(Of T, TEntity As {SD.LLBLGen.Pro.ORMSupportClasses.EntityBase2, SD.LLBLGen.Pro.ORMSupportClasses.IEntity2})

    Inherits isr.Core.SyncPublisherBase
    Implements IEntitySyncPublisher(Of TEntity)

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    Private _observer As IEntityObserver(Of TEntity)
    ''' <summary>
    ''' Constructs the class for linking to an <see cref="IEntityObserver(Of TEntity)">observer</see>.
    ''' </summary>
    ''' <param name="observer">Specifies reference to a class implementing the 
    ''' <see cref="IEntityObserver(Of TEntity)">observer</see> contract</param>
    ''' <param name="repository"></param>
    ''' <remarks></remarks>
    Protected Sub New(ByVal observer As IEntityObserver(Of TEntity), ByVal repository As IRepository(Of T))
        MyBase.New(observer)
        _observer = observer
        _Repository = repository
        _context = New SD.LLBLGen.Pro.ORMSupportClasses.Context
    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)

        If Not MyBase.IsDisposed Then

            Try

                If disposing Then

                    _context = Nothing
                    _Repository = Nothing
                    _observer = Nothing
                    _entity = Nothing

                End If

                ' Free shared unmanaged resources

            Finally

                MyBase.Dispose(disposing)

            End Try

        End If

    End Sub

#End Region

#Region " HELPERS "

    ''' <summary>
    ''' Builds prefetch path for connector.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected MustOverride Function BuildPrefetchPath() As SD.LLBLGen.Pro.ORMSupportClasses.IPrefetchPath2

#End Region

#Region " ENTITY NO PUBLISH "

    Public Function FetchEntity() As Boolean
        Using adapter As SD.LLBLGen.Pro.ORMSupportClasses.DataAccessAdapterBase = _Repository.DataAccessAdaptorGetter(True)
            Return Me.FetchEntity(adapter)
        End Using
    End Function

    ''' <summary>
    ''' Fetches an existing entity. Use this after save.
    ''' Requires having a <see cref="SD.LLBLGen.Pro.ORMSupportClasses.Context">context</see>.
    ''' </summary>
    ''' <param name="adapter">Specifies an active <see cref="SD.LLBLGen.Pro.ORMSupportClasses.DataAccessAdapterBase">adapter</see>.</param>
    ''' <returns>True if entity exists. Otherwise, a new entity is created.</returns>
    ''' <remarks>
    ''' Creates a new context if the entity is new. This ensures that related entities are fetched once and only once.
    ''' Does not publish the <see cref="OnEntityChanged">entity changed</see> event.
    ''' </remarks>
    Public Function FetchEntity(ByVal adapter As SD.LLBLGen.Pro.ORMSupportClasses.DataAccessAdapterBase) As Boolean
        If Me.Entity Is Nothing Then
            Return True
        Else
            Dim _wasAutoFetchEnabled As Boolean = Me.AutoFetchEnabled
            Try
                ' disable auto fetch because the property changed event fires during the fetch.
                Me.AutoFetchEnabled = False
                If Me.Entity.IsNew Then
                    _context = New SD.LLBLGen.Pro.ORMSupportClasses.Context
                End If
                adapter.FetchEntity(Me.Entity, Me.BuildPrefetchPath(), Me.Context)
                Return Not Me.Entity.IsNew
            Catch ex As SD.LLBLGen.Pro.ORMSupportClasses.ORMQueryExecutionException
                If Me.Entity Is Nothing Then
                    Me.HandleMessage(TraceEventType.Warning, "EXCEPTION OCCURRED FETCHING ENTITY", _
                                     "Exception occured fetching entity from the database -- entity is nothing.{0}Query Executed: {1}.{0}Details: {2}", _
                                     Environment.NewLine, ex.QueryExecuted, ex)
                Else
                    Dim fieldNames As String = String.Join(",", Me.Entity.GetPrimaryKeyFieldNames.ToArray)
                    Me.HandleMessage(TraceEventType.Warning, "EXCEPTION OCCURRED FETCHING ENTITY", _
                                     "Exception occured fetching entity '{1}' with keys '{2}' from the database.{0}Query Executed: {3}.{0}Details: {4}", _
                                     Environment.NewLine, Me.Entity, fieldNames, ex.QueryExecuted, ex)
                End If
            Finally
                Me.AutoFetchEnabled = _wasAutoFetchEnabled
            End Try
        End If
    End Function

    ''' <summary>
    ''' Reutrsn true if the speficifed entity exists in the database.
    ''' </summary>
    ''' <param name="adapter">Specifies an active <see cref="SD.LLBLGen.Pro.ORMSupportClasses.DataAccessAdapterBase">adapter</see>.</param>
    ''' <param name="entity">Specifies the entity to locate.</param>
    ''' <returns>True if entity exists. Otherwise, a new entity is created.</returns>
    ''' <remarks></remarks>
    Protected Function Find(ByVal adapter As SD.LLBLGen.Pro.ORMSupportClasses.DataAccessAdapterBase, ByVal entity As TEntity) As Boolean

        If entity Is Nothing Then
            Return False
        Else
            adapter.FetchEntity(entity)
            Return Not entity.IsNew
        End If

    End Function

    Private _Repository As IRepository(Of T)
    ''' <summary>
    ''' Gets reference to the repository entity.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected ReadOnly Property Repository() As IRepository(Of T)
        Get
            Return _Repository
        End Get
    End Property

    ''' <summary>
    ''' Saves the active entity and refetches it without publishing the 
    ''' <see cref="OnEntitySaved">Entity Saved</see> event.
    ''' </summary>
    ''' <returns>True if entity was save and successfuly refetched.</returns>
    ''' <remarks></remarks>
    Public Function SaveEntity() As Boolean
        Using adapter As SD.LLBLGen.Pro.ORMSupportClasses.DataAccessAdapterBase = _Repository.DataAccessAdaptorGetter(True)
            Return Me.SaveEntity(adapter)
        End Using
    End Function

    ''' <summary>
    ''' Saves the active entity and refetches it without raising any events.
    ''' </summary>
    ''' <returns>True if entity was save and successfuly refetched.</returns>
    ''' <remarks></remarks>
    Public Function SaveEntity(ByVal adapter As SD.LLBLGen.Pro.ORMSupportClasses.DataAccessAdapterBase) As Boolean

        Try

            Dim fieldNames As String = String.Join(",", Me.Entity.GetPrimaryKeyFieldNames.ToArray)
            Me.HandleMessage(TraceEventType.Verbose, "SAVING ENTITY", "Saving {0} '{1}'", Me.Entity, fieldNames)
            If adapter.SaveEntity(Me.Entity) Then
                If Me.FetchEntity(adapter) Then
                    Return True
                Else
                    Me.HandleMessage(TraceEventType.Warning, "FAILED FETCHING ENTITY AFTER SAVING", _
                                     "Failed fetching '{0}' '{1}' from the database after saving.", Me.Entity, fieldNames)
                    Return False
                End If
            Else
                Me.HandleMessage(TraceEventType.Warning, "FAILED SAVING ENTITY", _
                                 "Failed saving '{0}' '{1}' to the database.", Me.Entity, fieldNames)
                Return False
            End If
            Return True

        Catch ex As SD.LLBLGen.Pro.ORMSupportClasses.ORMQueryExecutionException

            Me.HandleMessage(TraceEventType.Warning, "EXCEPTION OCCURRED SAVING ENTITY", _
                             "Exception occurred saving entity '{1}' to the database.{0}Query Executed: {2}.{0}Details: {3}", _
                             Environment.NewLine, Me.Entity, ex.QueryExecuted, ex)
            Return False

        End Try

    End Function

#End Region

#Region " ENTITY WITH PUBLISH "

    Private _AutoFetchEnabled As Boolean
    ''' <summary>
    ''' Enables fetching from the changed event. 
    ''' </summary>
    ''' <remarks></remarks>
    Public Property AutoFetchEnabled() As Boolean Implements IEntitySyncPublisher(Of TEntity).AutoFetchEnabled
        Get
            Return _AutoFetchEnabled
        End Get
        Set(ByVal value As Boolean)
            _AutoFetchEnabled = value
        End Set
    End Property

    Private _context As SD.LLBLGen.Pro.ORMSupportClasses.Context
    ''' <summary>
    ''' Gets reference tot he general context class which provides uniquing supporting to 
    ''' the fetching process ensuring the each entity is loaded only once. 
    ''' </summary>
    ''' <remarks></remarks>
    Protected ReadOnly Property Context() As SD.LLBLGen.Pro.ORMSupportClasses.Context
        Get
            Return _context
        End Get
    End Property

    ''' <summary>
    ''' Deletes the selected entity.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Delete() As Boolean Implements IEntitySyncPublisher(Of TEntity).Delete
        Using adapter As SD.LLBLGen.Pro.ORMSupportClasses.DataAccessAdapterBase = _Repository.DataAccessAdaptorGetter(True)
            If adapter.DeleteEntity(Me.Entity) Then
                OnEntityChanged()
            End If
        End Using
    End Function

    ''' <summary>
    ''' Sets the entity.
    ''' </summary>
    ''' <param name="value"></param>
    ''' <remarks></remarks>
    Public Overridable Sub EntitySetter(ByVal value As TEntity) Implements IEntitySyncPublisher(Of TEntity).EntitySetter
        _entity = value
    End Sub

    Private WithEvents _entity As TEntity
    ''' <summary>
    ''' Returns reference to the entity.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overridable ReadOnly Property Entity() As TEntity Implements IEntitySyncPublisher(Of TEntity).Entity
        Get
            Return _entity
        End Get
    End Property

    ''' <summary>
    ''' Fetches an entity or clears the existing entity.
    ''' </summary>
    ''' <param name="adapter">Specifies an active <see cref="SD.LLBLGen.Pro.ORMSupportClasses.DataAccessAdapterBase">adapter</see>.</param>
    ''' <returns>True if entity exists. Otherwise, a new entity is created.</returns>
    ''' <remarks></remarks>
    Protected Function Fetch(ByVal adapter As SD.LLBLGen.Pro.ORMSupportClasses.DataAccessAdapterBase) As Boolean
        Me.FetchEntity(adapter)
        OnEntityChanged()
        Return Me.Entity Is Nothing OrElse Not Me.Entity.IsNew
    End Function

    ''' <summary>
    ''' Fetches an entity or clears the existing entity.
    ''' </summary>
    ''' <param name="adapter">Specifies an active <see cref="SD.LLBLGen.Pro.ORMSupportClasses.DataAccessAdapterBase">adapter</see>.</param>
    ''' <param name="entity">Specifies the entity to set and fetch.</param>
    ''' <returns>True if entity exists. Otherwise, a new entity is created.</returns>
    ''' <remarks></remarks>
    Protected Function Fetch(ByVal adapter As SD.LLBLGen.Pro.ORMSupportClasses.DataAccessAdapterBase, ByVal entity As TEntity) As Boolean
        Me.EntitySetter(entity)
        Return Me.Fetch(adapter)
    End Function

#If False Then
    ' 4102 will be needed when uypdating the calling libraries.
    ''' <summary>
    ''' Instantiates a new entity. Must be overriden
    ''' </summary><returns></returns>
    Public Overrides Function NewEntity() As Boolean
      OnEntityChanged()
      Return True
    End Function
#End If

    ''' <summary>
    ''' Reports that an entity was saved.
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overridable Sub OnEntitySaved(ByVal observer As IEntityObserver(Of TEntity))
        MyBase.NotifyObserver(Of TEntity)(Function(x As TEntity) observer.OnEntitySaved(x), Me.Entity)
    End Sub

    ''' <summary>
    ''' Updates the application entity data.
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overridable Sub OnEntityChanged()
        Me.Publish()
    End Sub

    ''' <summary>
    ''' Handles the change in primary key.
    ''' </summary>
    ''' <remarks></remarks>
    Protected MustOverride Sub OnPrimaryKeyChanged()

    ''' <summary>
    ''' Reports a property change.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub onPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Handles _entity.PropertyChanged
        If Me._entity.Fields(e.PropertyName) IsNot Nothing AndAlso Me._entity.Fields(e.PropertyName).IsPrimaryKey Then
            ' if pk changed, handle the change. This could be used to fetch a new entity. 
            Me.OnPrimaryKeyChanged()
        Else
            MyBase.NotifyObserver(Of TEntity)(Function(x As TEntity) _observer.OnEntityPropertyChanged(x), Me.Entity)
        End If
    End Sub

    ''' <summary>
    ''' Saves the active entity and refetches it without publishing the 
    ''' <see cref="OnEntitySaved">Entity Saved</see> event.
    ''' </summary>
    ''' <returns>True if entity was save and successfuly refetched.</returns>
    ''' <remarks></remarks>
    Public Overridable Overloads Function Save() As Boolean Implements IEntitySyncPublisher(Of TEntity).Save
        Using adapter As SD.LLBLGen.Pro.ORMSupportClasses.DataAccessAdapterBase = _Repository.DataAccessAdaptorGetter(True)
            Return Me.Save(adapter)
        End Using
    End Function

    ''' <summary>
    ''' Saves the active entity and refetches it.
    ''' </summary>
    ''' <param name="adapter">Specifies an active <see cref="SD.LLBLGen.Pro.ORMSupportClasses.DataAccessAdapterBase">adapter</see>.</param>
    ''' <returns>True if entity was save and successfuly refetched.</returns>
    ''' <remarks></remarks>
    Public Overridable Overloads Function Save(ByVal adapter As SD.LLBLGen.Pro.ORMSupportClasses.DataAccessAdapterBase) As Boolean Implements IEntitySyncPublisher(Of TEntity).Save

        If Me.SaveEntity(adapter) Then
            Me.OnEntitySaved(_observer)
            Me.OnEntityChanged()
            Return True
        Else
            Return False
        End If

    End Function

#End Region

#Region " PUBLISH "

    ''' <summary>
    ''' Displays and logs message.
    ''' </summary>
    ''' <param name="level">Messsage <see cref="TraceEventType">level</see></param>
    ''' <param name="synopsis">Short description of message</param>
    ''' <param name="format">A string format for formatting the message</param>
    ''' <param name="args">Paramater array for arguments</param>
    ''' <remarks></remarks>
    Protected Sub HandleMessage(ByVal level As TraceEventType, ByVal synopsis As String, ByVal format As String, ByVal ParamArray args() As Object)
        Dim message As New isr.Core.MessageEventArgs(level, level, synopsis, format, args)
        MyBase.NotifyObserver(Of isr.Core.MessageEventArgs)(Function(x As isr.Core.MessageEventArgs) _observer.OnMessageAvailable(x), message)
    End Sub

    ''' <summary>
    ''' Publishes the element information to the observer and the director. 
    ''' </summary>
    ''' <remarks></remarks>
    Public Overrides Sub Publish()
        MyBase.NotifyObserver(Of TEntity)(Function(x As TEntity) _observer.OnEntityChanged(x), Me.Entity)
    End Sub

#End Region

End Class
