﻿''' <summary>
''' Base class for anonymously publishing entity information for an entity that has one primary keys.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="10/03/2010" by="David Hary" revision="1.0.3928.x">
''' Created
''' </history>
<CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1005:AvoidExcessiveParametersOnGenericTypes")> _
Public MustInherit Class EntityAnonPublisherBase1(Of T, TEntity As {SD.LLBLGen.Pro.ORMSupportClasses.EntityBase2, SD.LLBLGen.Pro.ORMSupportClasses.IEntity2}, _
                                                  TPrimaryKey)

    Inherits EntityAnonPublisherBase(Of T, TEntity)
    Implements IEntityAnonPublisher1(Of TEntity, TPrimaryKey)

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Constructs the class using the <see cref="IRepository(Of T)">repository</see> 
    ''' for access to the <see cref="SD.LLBLGen.Pro.ORMSupportClasses.DataAccessAdapterBase">adapter</see> and repository id.
    ''' </summary>
    ''' <param name="repository">Reference to the object providing the repository id.</param>
    ''' <remarks></remarks>
    Protected Sub New(ByVal repository As IRepository(Of T))
        MyBase.New(repository)
    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)

        If Not MyBase.IsDisposed Then

            Try

                If disposing Then

                End If

                ' Free shared unmanaged resources

            Finally

                MyBase.Dispose(disposing)

            End Try

        End If

    End Sub

#End Region

#Region " ENTITY "

    ''' <summary>
    ''' Fetches an entity.
    ''' </summary>
    ''' <param name="primaryKey">Specifies the primary key</param>
    ''' <returns>True if entity exists. Otherwise, a new entity is created.</returns>
    ''' <remarks></remarks>
    Public MustOverride Overloads Function Fetch(ByVal primaryKey As TPrimaryKey) As Boolean Implements IEntityAnonPublisher1(Of TEntity, TPrimaryKey).Fetch

    ''' <summary>
    ''' Fetches an entity or clears the existing entity.
    ''' </summary>
    ''' <param name="adapter">Specifies an active <see cref="SD.LLBLGen.Pro.ORMSupportClasses.DataAccessAdapterBase">adapter</see>.</param>
    ''' <param name="primaryKey">Specifies the first primary key</param>
    ''' <returns>True if entity exists. Otherwise, a new entity is created.</returns>
    ''' <remarks></remarks>
    Public MustOverride Overloads Function Fetch(ByVal adapter As SD.LLBLGen.Pro.ORMSupportClasses.DataAccessAdapterBase, ByVal primaryKey As TPrimaryKey) As Boolean Implements IEntityAnonPublisher1(Of TEntity, TPrimaryKey).Fetch

    ''' <summary>
    ''' Finds an entity.
    ''' </summary>
    ''' <param name="primaryKey">Specifies the primary key</param>
    ''' <returns>True if entity exists.</returns>
    ''' <remarks></remarks>
    Public MustOverride Overloads Function Find(ByVal primaryKey As TPrimaryKey) As Boolean Implements IEntityAnonPublisher1(Of TEntity, TPrimaryKey).Find

#End Region

End Class
