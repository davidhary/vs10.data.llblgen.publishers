﻿Imports isr.Data.LLBLGen.EntityExtensions
''' <summary>
''' Base class for anonymously publishing entity information for an entity.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="10/03/2010" by="David Hary" revision="1.0.3928.x">
''' Created
''' </history>
Public MustInherit Class EntityAnonPublisherBase(Of T, TEntity As {SD.LLBLGen.Pro.ORMSupportClasses.EntityBase2, SD.LLBLGen.Pro.ORMSupportClasses.IEntity2})
    Inherits EntityPublisherBase
    Implements IEntityAnonPublisher(Of TEntity)

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Constructs the class.
    ''' </summary>
    ''' <param name="repository">Reference to the object providing the repository id.</param>
    ''' <remarks></remarks>
    Protected Sub New(ByVal repository As IRepository(Of T))
        MyBase.New()
        _Repository = repository
        _context = New SD.LLBLGen.Pro.ORMSupportClasses.Context
        _EntityName = ""
    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)

        If Not Me.IsDisposed Then

            Try

                If disposing Then

                    _context = Nothing

                    ' remove reference to the entity
                    _entity = Nothing


                End If

                ' Free shared unmanaged resources

            Finally

                MyBase.Dispose(disposing)

            End Try

        End If

    End Sub

#End Region

#Region " HELPERS "

    ''' <summary>
    ''' Builds prefetch path for connector.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected MustOverride Function BuildPrefetchPath() As SD.LLBLGen.Pro.ORMSupportClasses.IPrefetchPath2

#End Region

#Region " ENTITY NAME "

    Private _EntityName As String
    ''' <summary>
    ''' Gets or sets the name of the entity.
    ''' </summary>
    ''' <value>
    ''' The name of the entity.
    ''' </value>
    Public Overrides Property EntityName() As String
        Get
            If _entity Is Nothing Then
                Return _EntityName
            Else
                Return _entity.LLBLGenProEntityName
            End If
        End Get
        Set(ByVal value As String)
            _EntityName = value
        End Set
    End Property

#End Region

#Region " ENTITY NO PUBLISH "

    Public Function FetchEntity() As Boolean
        Using adapter As SD.LLBLGen.Pro.ORMSupportClasses.DataAccessAdapterBase = Me.Repository.DataAccessAdaptorGetter(True)
            Return Me.FetchEntity(adapter)
        End Using
    End Function

    ''' <summary>
    ''' Fetches an existing entity. Use this after save.
    ''' Requires having a <see cref="SD.LLBLGen.Pro.ORMSupportClasses.Context">context</see>.
    ''' </summary>
    ''' <param name="adapter">Specifies an active <see cref="SD.LLBLGen.Pro.ORMSupportClasses.DataAccessAdapterBase">adapter</see>.</param>
    ''' <returns>True if entity exists. Otherwise, a new entity is created.</returns>
    ''' <remarks>
    ''' Creates a new context if the entity is new. This ensures that related entities are fetched once and only once.
    ''' Does not publish the <see cref="OnEntityChanged">entity changed</see> event.
    ''' </remarks>
    Public Function FetchEntity(ByVal adapter As SD.LLBLGen.Pro.ORMSupportClasses.DataAccessAdapterBase) As Boolean
        If Me.Entity Is Nothing Then
            Return True
        Else
            Dim _wasAutoFetchEnabled As Boolean = Me.AutoFetchEnabled
            Try
                ' disable auto fetch because the property changed event fires during the fetch.
                Me.AutoFetchEnabled = False
                If Me.Entity.IsNew Then
                    _context = New SD.LLBLGen.Pro.ORMSupportClasses.Context
                End If
                adapter.FetchEntity(Me.Entity, Me.BuildPrefetchPath(), Me.Context)
                Return Not Me.Entity.IsNew
            Catch ex As SD.LLBLGen.Pro.ORMSupportClasses.ORMQueryExecutionException
                If Me.Entity Is Nothing Then
                    Me.HandleMessage(TraceEventType.Warning, "EXCEPTION OCCURRED FETCHING ENTITY", _
                                     "Exception occured fetching entity from the database -- entity is nothing.{0}Query Executed: {1}.{0}Details: {2}", _
                                     Environment.NewLine, ex.QueryExecuted, ex)
                Else
                    Dim fieldNames As String = String.Join(",", Me.Entity.GetPrimaryKeyFieldNames.ToArray)
                    Me.HandleMessage(TraceEventType.Warning, "EXCEPTION OCCURRED FETCHING ENTITY", _
                                     "Exception occured fetching entity '{1}' with keys '{2}' from the database.{0}Query Executed: {3}.{0}Details: {4}", _
                                     Environment.NewLine, Me.Entity, fieldNames, ex.QueryExecuted, ex)
                End If
            Finally
                Me.AutoFetchEnabled = _wasAutoFetchEnabled
            End Try
        End If
    End Function

    ''' <summary>
    ''' Reutrsn true if the speficifed entity exists in the database.
    ''' </summary>
    ''' <param name="adapter">Specifies an active <see cref="SD.LLBLGen.Pro.ORMSupportClasses.DataAccessAdapterBase">adapter</see>.</param>
    ''' <param name="entity">Specifies the entity to locate.</param>
    ''' <returns>True if entity exists. Otherwise, a new entity is created.</returns>
    ''' <remarks></remarks>
    Protected Function Find(ByVal adapter As SD.LLBLGen.Pro.ORMSupportClasses.DataAccessAdapterBase, ByVal entity As TEntity) As Boolean

        If entity Is Nothing Then
            Return False
        Else
            adapter.FetchEntity(entity)
            Return Not entity.IsNew
        End If

    End Function

    Private _Repository As IRepository(Of T)
    ''' <summary>
    ''' Gets reference to the repository entity.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected ReadOnly Property Repository() As IRepository(Of T)
        Get
            Return _Repository
        End Get
    End Property

    ''' <summary>
    ''' Saves the active entity and refetches it without publishing the 
    ''' <see cref="OnEntitySaved">Entity Saved</see> event.
    ''' </summary>
    ''' <returns>True if entity was save and successfuly refetched.</returns>
    ''' <remarks></remarks>
    Public Overrides Function SaveEntity() As Boolean
        Using adapter As SD.LLBLGen.Pro.ORMSupportClasses.DataAccessAdapterBase = Me.Repository.DataAccessAdaptorGetter(True)
            Return Me.SaveEntity(adapter)
        End Using
    End Function

    ''' <summary>
    ''' Saves the active entity and refetches it without raising any events.
    ''' </summary>
    ''' <returns>True if entity was save and successfuly refetched.</returns>
    ''' <remarks></remarks>
    Public Overloads Function SaveEntity(ByVal adapter As SD.LLBLGen.Pro.ORMSupportClasses.DataAccessAdapterBase) As Boolean

        Try

            Dim fieldNames As String = String.Join(",", Me.Entity.GetPrimaryKeyFieldNames.ToArray)
            Me.HandleMessage(TraceEventType.Verbose, "SAVING ENTITY", "Saving {0} '{1}'", Me.Entity, fieldNames)
            If adapter.SaveEntity(Me.Entity) Then
                If Me.FetchEntity(adapter) Then
                    Return True
                Else
                    Me.HandleMessage(TraceEventType.Warning, "FAILED FETCHING ENTITY AFTER SAVING", _
                                     "Failed fetching '{0}' '{1}' from the database after saving.", Me.Entity, fieldNames)
                    Return False
                End If
            Else
                Me.HandleMessage(TraceEventType.Warning, "FAILED SAVING ENTITY", _
                                 "Failed saving '{0}' '{1}' to the database.", Me.Entity, fieldNames)
                Return False
            End If
            Return True

        Catch ex As SD.LLBLGen.Pro.ORMSupportClasses.ORMQueryExecutionException

            Me.HandleMessage(TraceEventType.Warning, "EXCEPTION OCCURRED SAVING ENTITY", _
                             "Exception occurred saving entity '{1}' to the database.{0}Query Executed: {2}.{0}Details: {3}", _
                             Environment.NewLine, Me.Entity, ex.QueryExecuted, ex)
            Return False

        End Try

    End Function

#End Region

#Region " ENTITY WITH PUBLISH "

    Private _context As SD.LLBLGen.Pro.ORMSupportClasses.Context
    ''' <summary>
    ''' Gets reference tot he general context class which provides uniquing supporting to 
    ''' the fetching process ensuring the each entity is loaded only once. 
    ''' </summary>
    ''' <remarks></remarks>
    Protected ReadOnly Property Context() As SD.LLBLGen.Pro.ORMSupportClasses.Context
        Get
            Return _context
        End Get
    End Property

    ''' <summary>
    ''' Deletes the selected entity.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overrides Function Delete() As Boolean
        Using adapter As SD.LLBLGen.Pro.ORMSupportClasses.DataAccessAdapterBase = Me.Repository.DataAccessAdaptorGetter(True)
            If adapter.DeleteEntity(Me.Entity) Then
                OnEntityChanged()
            End If
        End Using
    End Function

    ''' <summary>
    ''' Sets the entity.
    ''' </summary>
    ''' <param name="value"></param>
    ''' <remarks></remarks>
    Public Overridable Sub EntitySetter(ByVal value As TEntity) Implements IEntityAnonPublisher(Of TEntity).EntitySetter
        _entity = value
        'Dim name As String = _entity.LLBLGenProEntityName
    End Sub

    Private WithEvents _entity As TEntity
    ''' <summary>
    ''' Returns reference to the entity.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overridable ReadOnly Property Entity() As TEntity Implements IEntityAnonPublisher(Of TEntity).Entity
        Get
            Return _entity
        End Get
    End Property

    ''' <summary>
    ''' Fetches an entity or clears the existing entity.
    ''' </summary>
    ''' <param name="adapter">Specifies an active <see cref="SD.LLBLGen.Pro.ORMSupportClasses.DataAccessAdapterBase">adapter</see>.</param>
    ''' <returns>True if entity exists. Otherwise, a new entity is created.</returns>
    ''' <remarks></remarks>
    Protected Function Fetch(ByVal adapter As SD.LLBLGen.Pro.ORMSupportClasses.DataAccessAdapterBase) As Boolean
        Me.FetchEntity(adapter)
        OnEntityChanged()
        Return Me.Entity Is Nothing OrElse Not Me.Entity.IsNew
    End Function

    ''' <summary>
    ''' Fetches an entity or clears the existing entity.
    ''' </summary>
    ''' <param name="adapter">Specifies an active <see cref="SD.LLBLGen.Pro.ORMSupportClasses.DataAccessAdapterBase">adapter</see>.</param>
    ''' <param name="entity">Specifies the entity to set and fetch.</param>
    ''' <returns>True if entity exists. Otherwise, a new entity is created.</returns>
    ''' <remarks></remarks>
    Protected Function Fetch(ByVal adapter As SD.LLBLGen.Pro.ORMSupportClasses.DataAccessAdapterBase, ByVal entity As TEntity) As Boolean
        Me.EntitySetter(entity)
        Return Me.Fetch(adapter)
    End Function

    ''' <summary>
    ''' Saves the active entity and refetches it without publishing the 
    ''' <see cref="OnEntitySaved">Entity Saved</see> event.
    ''' </summary>
    ''' <returns>True if entity was save and successfuly refetched.</returns>
    ''' <remarks></remarks>
    Public Overloads Overrides Function Save() As Boolean
        Using adapter As SD.LLBLGen.Pro.ORMSupportClasses.DataAccessAdapterBase = Me.Repository.DataAccessAdaptorGetter(True)
            Return Me.Save(adapter)
        End Using
    End Function

#If False Then
    ' 4102 will be needed when uypdating the calling libraries.
    ''' <summary>
    ''' Instantiates a new entity. Must be overriden
    ''' </summary><returns></returns>
    Public Overrides Function NewEntity() As Boolean
      OnEntityChanged()
      Return True
    End Function
#End If

    ''' <summary>
    ''' Saves the active entity and refetches it.
    ''' </summary>
    ''' <param name="adapter">Specifies an active <see cref="SD.LLBLGen.Pro.ORMSupportClasses.DataAccessAdapterBase">adapter</see>.</param>
    ''' <returns>True if entity was save and successfuly refetched.</returns>
    ''' <remarks></remarks>
    Public Overridable Overloads Function Save(ByVal adapter As SD.LLBLGen.Pro.ORMSupportClasses.DataAccessAdapterBase) As Boolean Implements IEntityAnonPublisher(Of TEntity).Save

        If Me.SaveEntity(adapter) Then
            Me.OnEntitySaved()
            Me.OnEntityChanged()
            Return True
        Else
            Return False
        End If

    End Function

#End Region

#Region " EVENTS "

    ''' <summary>
    ''' Reports a property change.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub _PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Handles _entity.PropertyChanged
        If Me._entity.Fields(e.PropertyName) IsNot Nothing AndAlso Me._entity.Fields(e.PropertyName).IsPrimaryKey Then
            ' if pk changed, handle the change. This could be used to fetch a new entity. 
            Me.OnPrimaryKeyChanged()
        Else
            MyBase.OnPropertyChanged(e)
        End If
    End Sub

#End Region

End Class
