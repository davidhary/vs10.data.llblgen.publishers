﻿''' <summary>
''' Defines the contract implemented by the entity anonymous publisher for an entity.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="10/03/2010" by="David Hary" revision="1.0.3928.x">
''' Created
''' </history>
Public Interface IEntityAnonPublisher(Of TEntity As {SD.LLBLGen.Pro.ORMSupportClasses.EntityBase2, SD.LLBLGen.Pro.ORMSupportClasses.IEntity2})
    Inherits IDisposable, IEntityPublisher

    ''' <summary>
    ''' Sets the entity.
    ''' </summary>
    ''' <param name="value"></param>
    ''' <remarks></remarks>
    Sub EntitySetter(ByVal value As TEntity)

    ''' <summary>
    ''' Returns reference to the entity.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ReadOnly Property Entity() As TEntity

    ''' <summary>
    ''' Saves the active entity and refetches it.
    ''' </summary>
    ''' <param name="adapter">Specifies an active <see cref="SD.LLBLGen.Pro.ORMSupportClasses.DataAccessAdapterBase">adapter</see>.</param>
    ''' <returns>True if entity was save and successfuly refetched.</returns>
    ''' <remarks></remarks>
    Overloads Function Save(ByVal adapter As SD.LLBLGen.Pro.ORMSupportClasses.DataAccessAdapterBase) As Boolean

End Interface
