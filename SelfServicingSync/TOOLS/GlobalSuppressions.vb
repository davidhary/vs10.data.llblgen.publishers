Imports System.Diagnostics.CodeAnalysis

#Region " INSTRUCTIONS "

' This module provides assembly level (global) CodeAnalysis suppressions for FxCop.

' While static code analysis with FxCop is excellent for catching many common
' and not so common code errors, there are some things that it flags that
' do not always apply to the project at hand. For those cases, FxCop allows
' you to exclude the message (and optionally give a justification reason for
' excluding it). However, those exclusions are stored only in the FxCop
' project file. In the 2.0 version of the .NET framework, Microsoft introduced
' SuppressMessageAttribute, which is used primarily by the version of FxCop
' that is built in to Visual Studio. As this built-in functionality is not
' included in all versions of Visual Studio, we have opted to continue
' using the standalone version of FxCop. 

' In order for this version to recognize SupressMessageAttribute, the
' CODE_ANALYSIS symbol must be defined as follows:

' In Visual Studio Non-Team system you have to enable your project to recognize the 
' SuppressMessage attribute by FIRST adding a condition compilation symbol.
' 1. In Solution Explorer, right-click your project and choose Properties
' 2. In the Properties window, choose the Compile tab and click Advanced Compile Options
' 3. In the Custom constants text box enter CODE_ANALYSIS

#End Region

#Region "CA1704:IdentifiersShouldBeSpelledCorrectly"

<Module: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope:="type", Target:="isr.Data.LLBLGen.SelfServicing.EntityMultiSyncPublisherBaseSub`2", MessageId:="Multi")> 
<Module: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope:="type", Target:="isr.Data.LLBLGen.SelfServicing.IEntityMultiSyncPublisherSub`2", MessageId:="Multi")> 
<Module: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope:="type", Target:="isr.Data.LLBLGen.SelfServicing.IEntityMultiSyncPublisher3`4", MessageId:="Multi")> 
<Module: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope:="type", Target:="isr.Data.LLBLGen.SelfServicing.EntityMultiSyncPublisherBase1`2", MessageId:="Multi")> 
<Module: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope:="type", Target:="isr.Data.LLBLGen.SelfServicing.IEntityMultiSyncPublisher2Sub`4", MessageId:="Multi")> 
<Module: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope:="type", Target:="isr.Data.LLBLGen.SelfServicing.IEntityMultiSyncPublisher`1", MessageId:="Multi")> 
<Module: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope:="type", Target:="isr.Data.LLBLGen.SelfServicing.EntityMultiSyncPublisherBase2Sub`4", MessageId:="Multi")> 
<Module: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope:="type", Target:="isr.Data.LLBLGen.SelfServicing.EntityMultiSyncPublisherBase3`4", MessageId:="Multi")> 
<Module: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope:="type", Target:="isr.Data.LLBLGen.SelfServicing.IEntityMultiSyncPublisher1Sub`3", MessageId:="Multi")> 
<Module: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope:="type", Target:="isr.Data.LLBLGen.SelfServicing.EntityMultiSyncPublisherBase1Sub`3", MessageId:="Multi")> 
<Module: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope:="type", Target:="isr.Data.LLBLGen.SelfServicing.IEntityMultiSyncPublisher2`3", MessageId:="Multi")> 
<Module: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope:="type", Target:="isr.Data.LLBLGen.SelfServicing.IEntityMultiSyncPublisher1`2", MessageId:="Multi")> 
<Module: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope:="type", Target:="isr.Data.LLBLGen.SelfServicing.EntityMultiSyncPublisherBase2`3", MessageId:="Multi")> 
<Module: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope:="type", Target:="isr.Data.LLBLGen.SelfServicing.EntityMultiSyncPublisherBase`1", MessageId:="Multi")> 
<Module: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope:="type", Target:="isr.Data.LLBLGen.SelfServicing.EntitySyncMultiPublisherBase3`4", MessageId:="Multi")> 

#End Region

