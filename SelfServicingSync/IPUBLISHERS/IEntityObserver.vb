﻿''' <summary>
''' Defines the contract implemented by the entity observer.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="10/03/2010" by="David Hary" revision="1.0.3928.x">
''' Created
''' </history>
Public Interface IEntityObserver(Of TEntity As {SD.LLBLGen.Pro.ORMSupportClasses.EntityBase, SD.LLBLGen.Pro.ORMSupportClasses.IEntity})

    Inherits System.ComponentModel.ISynchronizeInvoke, isr.Core.IMessageObserver

    ''' <summary>
    ''' Handles the selection of a new entity.
    ''' </summary>
    ''' <param name="entity">Specifies the entity or nothing if none changed.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function OnEntityChanged(ByVal entity As TEntity) As Boolean

    ''' <summary>
    ''' Handles a change of an existing entity.
    ''' </summary>
    ''' <param name="entity">Specifies the entity or nothing if none changed.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function OnEntityPropertyChanged(ByVal entity As TEntity) As Boolean

    ''' <summary>
    ''' Handles saving or updating an Entity.
    ''' </summary>
    ''' <param name="entity">Specifies the entity or nothing if none saved.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function OnEntitySaved(ByVal entity As TEntity) As Boolean

    ''' <summary>
    ''' Handles a change of a member entity collection.
    ''' </summary>
    ''' <param name="memberEntities">Specifies the entities that changed.</param>
    ''' <param name="e">Specifies the <see cref="System.ComponentModel.ListChangedEventArgs">list changed event arguments.</see></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Overloads Function OnMemberEntitiesChanged(ByVal memberEntities As SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection, _
                                               ByVal e As System.ComponentModel.ListChangedEventArgs) As Boolean


End Interface
