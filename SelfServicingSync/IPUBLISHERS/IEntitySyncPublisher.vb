﻿''' <summary>
''' Defines the contract implemented by the entity sync publisher for an entity.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="10/03/2010" by="David Hary" revision="1.0.3928.x">
''' created
''' </history>
Public Interface IEntitySyncPublisher(Of TEntity As {SD.LLBLGen.Pro.ORMSupportClasses.EntityBase, SD.LLBLGen.Pro.ORMSupportClasses.IEntity})
    Inherits isr.Core.ISyncPublisher

    ''' <summary>
    ''' Returns reference to the entity.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ReadOnly Property Entity() As TEntity

#Region " ENTITY NO PUBLISH "

    ''' <summary>
    ''' Saves the active entity.
    ''' </summary>
    ''' <returns>True if entity was save and successfuly refetched.</returns>
    ''' <remarks>
    ''' Does not publish.
    ''' </remarks>
    Function SaveEntity() As Boolean

#End Region

#Region " ENTITY WITH PUBLISH "

    ''' <summary>
    ''' Enables fetching from the changed event. 
    ''' </summary>
    ''' <remarks></remarks>
    Property AutoFetchEnabled() As Boolean

    ''' <summary>
    ''' Deletes the selected entity.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function Delete() As Boolean

    ''' <summary>
    ''' Saves the active entity.
    ''' </summary>
    ''' <returns>True if entity was save and successfuly refetched.</returns>
    ''' <remarks></remarks>
    Function Save() As Boolean

#End Region

End Interface
