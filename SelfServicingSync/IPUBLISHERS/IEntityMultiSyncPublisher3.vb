﻿''' <summary>
''' Defines the contract implemented by the entity sync publisher to multiple observers 
''' for an entity having a primary key consisting of three fields.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="10/03/2010" by="David Hary" revision="1.0.3928.x">
''' Created
''' </history>
<CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1005:AvoidExcessiveParametersOnGenericTypes")> _
Public Interface IEntityMultiSyncPublisher3(Of TEntity As {SD.LLBLGen.Pro.ORMSupportClasses.EntityBase, SD.LLBLGen.Pro.ORMSupportClasses.IEntity}, _
                                        TPrimaryKey1, TPrimaryKey2, TPrimaryKey3)
    Inherits IEntityMultiSyncPublisher(Of TEntity)

    ''' <summary>
    ''' Fetches an entity or clear exiting entity.
    ''' </summary>
    ''' <param name="primaryKey1">Specifies the first primary key</param>
    ''' <param name="primaryKey2">Specifies the second primary key</param>
    ''' <param name="primaryKey3">Specifies the third primary key</param>
    ''' <returns>True if entity exists. Otherwise, a new entity is created.</returns>
    ''' <remarks></remarks>
    Function Fetch(ByVal primaryKey1 As TPrimaryKey1, ByVal primaryKey2 As TPrimaryKey2, ByVal primaryKey3 As TPrimaryKey3) As Boolean

    ''' <summary>
    ''' Finds an entity.
    ''' </summary>
    ''' <param name="primaryKey1">Specifies the first primary key</param>
    ''' <param name="primaryKey2">Specifies the second primary key</param>
    ''' <param name="primaryKey3">Specifies the third primary key</param>
    ''' <returns>True if entity exists.</returns>
    ''' <remarks>Find does not trigger the entity changed event</remarks>
    Function Find(ByVal primaryKey1 As TPrimaryKey1, ByVal primaryKey2 As TPrimaryKey2, ByVal primaryKey3 As TPrimaryKey3) As Boolean

End Interface
