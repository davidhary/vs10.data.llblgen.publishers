﻿''' <summary>
''' Defines the contract implemented by the entity sync publisher to multiple observers.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="10/03/2010" by="David Hary" revision="1.0.3928.x">
''' Created
''' </history>
Public Interface IEntityMultiSyncPublisher(Of TEntity As {SD.LLBLGen.Pro.ORMSupportClasses.EntityBase, SD.LLBLGen.Pro.ORMSupportClasses.IEntity})
    Inherits IEntitySyncPublisher(Of TEntity)

    ''' <summary>
    ''' Adds an observer.
    ''' </summary>
    ''' <param name="observer">Specifies reference to a class implementing the 
    ''' <see cref="IEntityObserver">observer</see> contract</param>
    ''' <returns>True if observer added.</returns>
    ''' <remarks></remarks>
    Function AddObserver(ByVal observer As IEntityObserver(Of TEntity)) As Boolean

End Interface
