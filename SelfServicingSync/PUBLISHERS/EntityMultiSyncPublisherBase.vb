﻿Imports isr.Data.LLBLGen.EntityExtensions
''' <summary>
''' Base class for sync publishing entity information to multiple observers.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="10/03/2010" by="David Hary" revision="1.0.3928.x">
''' Created
''' </history>
Public MustInherit Class EntityMultiSyncPublisherBase(Of TEntity As {SD.LLBLGen.Pro.ORMSupportClasses.EntityBase, SD.LLBLGen.Pro.ORMSupportClasses.IEntity})

    Inherits EntitySyncPublisherBase(Of TEntity)
    Implements IEntityMultiSyncPublisher(Of TEntity)

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Constructs the class for linking to an <see cref="IEntityObserver(Of TEntity)">observer</see>.
    ''' </summary>
    ''' <param name="observer">Specifies reference to a class implementing the 
    ''' <see cref="IEntityObserver(Of TEntity)">observer</see> contract</param>
    ''' <remarks></remarks>
    Protected Sub New(ByVal observer As IEntityObserver(Of TEntity))
        MyBase.New(observer)
        _addObserver(observer)
    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)

        If Not MyBase.IsDisposed Then

            Try

                If disposing Then

                    If _observers IsNot Nothing Then
                        _observers.Clear()
                        _observers = Nothing
                    End If

                End If

                ' Free shared unmanaged resources

            Finally

                MyBase.Dispose(disposing)

            End Try

        End If

    End Sub

#End Region

#Region " OBSERVERS "

    Private _observers As Collections.Generic.List(Of IEntityObserver(Of TEntity))
    ''' <summary>
    ''' Adds an observer.
    ''' </summary>
    ''' <param name="observer">Specifies reference to a class implementing the 
    ''' <see cref="IEntityObserver(Of TEntity)">observer</see> contract</param>
    ''' <returns>True if observer added.</returns>
    ''' <remarks></remarks>
    Private Function _addObserver(ByVal observer As IEntityObserver(Of TEntity)) As Boolean
        If _observers Is Nothing Then
            _observers = New Collections.Generic.List(Of IEntityObserver(Of TEntity))
            '_primaryObserver = observer
        End If
        _observers.Add(observer)
        Return True
    End Function

    ''' <summary>
    ''' Adds an observer.
    ''' </summary>
    ''' <param name="observer">Specifies reference to a class implementing the 
    ''' <see cref="IEntityObserver(Of TEntity)">observer</see> contract</param>
    ''' <returns>True if observer added.</returns>
    ''' <remarks></remarks>
    Public Function AddObserver(ByVal observer As IEntityObserver(Of TEntity)) As Boolean Implements IEntityMultiSyncPublisher(Of TEntity).AddObserver
        If _addObserver(observer) Then
            Me.Publish(observer)
        End If
    End Function

#End Region

#Region " ENTITY "

    ''' <summary>
    ''' Reports that an entity was saved.
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overloads Overrides Sub OnEntitySaved()
        For Each observer As IEntityObserver(Of TEntity) In _observers
            MyBase.OnEntitySaved(observer)
        Next
    End Sub

#End Region

#Region " PUBLISH "

    ''' <summary>
    ''' Displays and logs message.
    ''' </summary>
    ''' <param name="level">Messsage <see cref="TraceEventType">level</see></param>
    ''' <param name="synopsis">Short description of message</param>
    ''' <param name="format">A string format for formatting the message</param>
    ''' <param name="args">Paramater array for arguments</param>
    ''' <remarks></remarks>
    Protected Overrides Sub HandleMessage(ByVal level As TraceEventType, ByVal synopsis As String, ByVal format As String, ByVal ParamArray args() As Object)
        Dim message As New isr.Core.MessageEventArgs(level, level, synopsis, format, args)
        For Each observer As Core.IMessageObserver In _observers
            MyBase.HandleMessage(observer, message)
        Next
    End Sub

    ''' <summary>
    ''' Publishes the element information to the observer and the director. 
    ''' </summary>
    ''' <remarks></remarks>
    Public Overrides Sub Publish()
        For Each observer As IEntityObserver(Of TEntity) In _observers
            Me.Publish(observer)
        Next
    End Sub

#End Region

End Class
