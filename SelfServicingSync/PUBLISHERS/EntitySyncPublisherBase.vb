﻿Imports isr.Data.LLBLGen.EntityExtensions
''' <summary>
''' Base class for sync publishing entity information to a single observer.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="10/03/2010" by="David Hary" revision="1.0.3928.x">
''' Created
''' </history>
Public MustInherit Class EntitySyncPublisherBase(Of TEntity As {SD.LLBLGen.Pro.ORMSupportClasses.EntityBase, SD.LLBLGen.Pro.ORMSupportClasses.IEntity})
    Inherits isr.Core.SyncPublisherBase
    Implements IEntitySyncPublisher(Of TEntity)

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Constructs the class for linking to an <see cref="IEntityObserver(Of TEntity)">observer</see>.
    ''' </summary>
    ''' <param name="observer">Specifies reference to a class implementing the 
    ''' <see cref="IEntityObserver(Of TEntity)">observer</see> contract</param>
    ''' <remarks></remarks>
    Protected Sub New(ByVal observer As IEntityObserver(Of TEntity))
        MyBase.New(observer)
        _observer = observer
    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)

        If Not MyBase.IsDisposed Then

            Try

                If disposing Then

                    If _observer IsNot Nothing Then
                        _observer = Nothing
                    End If
                    _entity = Nothing

                End If

                ' Free shared unmanaged resources

            Finally

                MyBase.Dispose(disposing)

            End Try

        End If

    End Sub

#End Region

#Region " HELPERS "

    ''' <summary>
    ''' Builds prefetch path for the entity.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected MustOverride Function BuildPrefetchPath() As SD.LLBLGen.Pro.ORMSupportClasses.IPrefetchPath

#End Region

#Region " OBSERVERS "

    Private _observer As IEntityObserver(Of TEntity)

#End Region

#Region " ENTITY -- BASE METHODS - NO PUBLISHING "

    ''' <summary>
    ''' Saves the active entity.
    ''' </summary>
    ''' <returns>True if entity was save and successfuly refetched.</returns>
    ''' <remarks></remarks>
    Protected Function SaveEntity() As Boolean Implements IEntitySyncPublisher(Of TEntity).SaveEntity

        Try

            Dim fieldNames As String = String.Join(",", Me.Entity.GetPrimaryKeyFieldNames.ToArray)
            Me.HandleMessage(TraceEventType.Verbose, "SAVING ENTITY", "Saving {0} '{1}'", Me.Entity, fieldNames)
            If Me.Entity.Save() Then
                ' refetch to keep entity active.
                If Me.Entity.Refetch() Then
                    Return True
                Else
                    Me.HandleMessage(TraceEventType.Warning, "FAILED FETCHING ENTITY AFTER SAVING", _
                                     "Failed fetching '{0}' '{1}' from the database after saving.", Me.Entity, fieldNames)
                    Return False
                End If
            Else
                Me.HandleMessage(TraceEventType.Warning, "FAILED SAVING ENTITY", _
                                 "Failed saving '{0}' '{1}' to the database.", Me.Entity, fieldNames)
                Return False
            End If
            Return False

        Catch ex As SD.LLBLGen.Pro.ORMSupportClasses.ORMQueryExecutionException

            Me.HandleMessage(TraceEventType.Warning, "EXCEPTION OCCURRED SAVING ENTITY", _
                             "Exception occured saving '{1}' to the database.{0}Query Executed: {2}.{0}Details: {3}", _
                             Environment.NewLine, Me.Entity, ex.QueryExecuted, ex)
            Return False

        End Try

    End Function

#End Region

#Region " ENTITY "

    Private _AutoFetchEnabled As Boolean
    ''' <summary>
    ''' Enables fetching from the changed event. 
    ''' </summary>
    ''' <remarks></remarks>
    Public Property AutoFetchEnabled() As Boolean Implements IEntitySyncPublisher(Of TEntity).AutoFetchEnabled
        Get
            Return _AutoFetchEnabled
        End Get
        Set(ByVal value As Boolean)
            _AutoFetchEnabled = value
        End Set
    End Property

    ''' <summary>
    ''' Deletes the selected entity.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overridable Function Delete() As Boolean Implements IEntitySyncPublisher(Of TEntity).Delete
        If Me.Entity.Delete() Then
            Me.EntitySetter(Nothing)
            OnEntityChanged()
        End If
        Return True
    End Function

    ''' <summary>
    ''' Sets the entity.
    ''' </summary>
    ''' <param name="value"></param>
    ''' <remarks></remarks>
    Public Sub EntitySetter(ByVal value As TEntity)
        _entity = value
    End Sub

    Private WithEvents _entity As TEntity
    ''' <summary>
    ''' Returns reference to the entity.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Entity() As TEntity Implements IEntitySyncPublisher(Of TEntity).Entity
        Get
            Return _entity
        End Get
    End Property

#If False Then
    ' 4102 will be needed when uypdating the calling libraries.
    ''' <summary>
    ''' Instantiates a new entity. Must be overriden
    ''' </summary><returns></returns>
    Public Overrides Function NewEntity() As Boolean
      OnEntityChanged()
      Return True
    End Function
#End If

    ''' <summary>
    ''' Saves the active entity.
    ''' </summary>
    ''' <returns>True if entity was save and successfuly refetched.</returns>
    ''' <remarks></remarks>
    Public Function Save() As Boolean Implements IEntitySyncPublisher(Of TEntity).Save

        If Me.SaveEntity Then
            Me.OnEntitySaved()
            Me.OnEntityChanged()
        End If

    End Function

#End Region

#Region " PUBLISH "

    ''' <summary>
    ''' Reports that an entity was saved.
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overridable Sub OnEntitySaved(ByVal observer As IEntityObserver(Of TEntity))
        MyBase.NotifyObserver(Of TEntity)(Function(x As TEntity) observer.OnEntitySaved(x), Me.Entity)
    End Sub

    ''' <summary>
    ''' Reports that an entity was saved.
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overridable Sub OnEntitySaved()
        Me.OnEntitySaved(_observer)
    End Sub

    ''' <summary>
    ''' Updates the application entity data.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub _OnEntityChanged()
        Me.Publish()
    End Sub

    ''' <summary>
    ''' Updates the application entity data.
    ''' </summary>
    ''' <remarks></remarks>
    Public Overloads Sub OnEntityChanged()
        _OnEntityChanged()
    End Sub

    ''' <summary>
    ''' Reports that an entity property changed indicating that the entity is now dirty and needs to be saved.
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub OnPropertyChanged(ByVal observer As IEntityObserver(Of TEntity))
        MyBase.NotifyObserver(Of TEntity)(Function(x As TEntity) observer.OnEntityPropertyChanged(x), Me.Entity)
    End Sub

    ''' <summary>
    ''' Handles the change in primary key.
    ''' </summary>
    ''' <remarks></remarks>
    Protected MustOverride Sub OnPrimaryKeyChanged()

    ''' <summary>
    ''' Reports a property change.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub OnPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Handles _entity.PropertyChanged
        If Me._entity.Fields(e.PropertyName) IsNot Nothing AndAlso Me._entity.Fields(e.PropertyName).IsPrimaryKey Then
            ' if pk changed, handle the change. This could be used to fetch a new entity. 
            Me.OnPrimaryKeyChanged()
        Else
            Me.OnPropertyChanged(_observer)
        End If
    End Sub

#End Region

#Region " MEMBER ENTITY COLLECTION CHANGES "

    Private WithEvents _MemberEntities As SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection
    ''' <summary>
    ''' Updates the list of sub entities that is monitored for changes.
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overridable Overloads Sub OnEntityChanged(ByVal memberEntities As SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection)
        _MemberEntities = memberEntities
        _OnEntityChanged()
    End Sub
    ''' <summary>
    ''' Notifies the observer of changes in the list of depending entities.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub _MemberEntities_ListChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ListChangedEventArgs) Handles _MemberEntities.ListChanged
        Me.OnMemberEntityCollectionChanged(e)
    End Sub

    ''' <summary>
    ''' Handles the change of the dependent list.
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overridable Sub OnMemberEntityCollectionChanged(ByVal e As System.ComponentModel.ListChangedEventArgs)
        MyBase.NotifyObserver(Of SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection,  _
                                 System.ComponentModel.ListChangedEventArgs)(Function(x As SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection, _
                                                                                      y As System.ComponentModel.ListChangedEventArgs) _observer.OnMemberEntitiesChanged(x, y), _MemberEntities, e)
    End Sub

#End Region

#Region " PUBLISH "

    ''' <summary>
    ''' Displays the message on the specific obserser.
    ''' </summary>
    ''' <param name="observer">Specifies the <see cref="Core.IMessageObserver">observer.</see></param>
    ''' <param name="message">Specifies the <see cref="isr.Core.MessageEventArgs">message.</see></param>
    ''' <remarks></remarks>
    Protected Sub HandleMessage(ByVal observer As Core.IMessageObserver, ByVal message As isr.Core.MessageEventArgs)
        MyBase.NotifyObserver(Of isr.Core.MessageEventArgs)(Function(x As isr.Core.MessageEventArgs) observer.OnMessageAvailable(x), message)
    End Sub

    ''' <summary>
    ''' Displays and logs message.
    ''' </summary>
    ''' <param name="level">Messsage <see cref="TraceEventType">level</see></param>
    ''' <param name="synopsis">Short description of message</param>
    ''' <param name="format">A string format for formatting the message</param>
    ''' <param name="args">Paramater array for arguments</param>
    ''' <remarks></remarks>
    Protected Overridable Sub HandleMessage(ByVal level As TraceEventType, ByVal synopsis As String, ByVal format As String, ByVal ParamArray args() As Object)
        Dim message As New isr.Core.MessageEventArgs(level, level, synopsis, format, args)
        Me.HandleMessage(_observer, message)
    End Sub

    ''' <summary>
    ''' Publishes the element information to the observer and the director. 
    ''' </summary>
    ''' <remarks></remarks>
    Public Overrides Sub Publish()
        Me.Publish(_observer)
    End Sub

    ''' <summary>
    ''' Publishes the element information to the observer. 
    ''' </summary>
    ''' <param name="observer">Specifies the <see cref="IEntityObserver(Of TEntity)">observer.</see></param>
    ''' <remarks></remarks>
    Protected Overridable Overloads Sub Publish(ByVal observer As IEntityObserver(Of TEntity))
        MyBase.NotifyObserver(Of TEntity)(Function(x As TEntity) observer.OnEntityChanged(x), Me.Entity)
    End Sub

#End Region

End Class
