﻿''' <summary>
''' Base class for sync publishing entity information to multiple observers
''' for an entity that has a primary key consisting of two fields.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="10/03/2010" by="David Hary" revision="1.0.3928.x">
''' Created
''' </history>
<CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1005:AvoidExcessiveParametersOnGenericTypes")> _
Public MustInherit Class EntityMultiSyncPublisherBase2(Of TEntity As {SD.LLBLGen.Pro.ORMSupportClasses.EntityBase, SD.LLBLGen.Pro.ORMSupportClasses.IEntity}, _
                                                 TPrimaryKey1, TPrimaryKey2)

    Inherits EntityMultiSyncPublisherBase(Of TEntity)
    Implements IEntityMultiSyncPublisher2(Of TEntity, TPrimaryKey1, TPrimaryKey2)

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Constructs the class for linking to an <see cref="IEntityObserver">observer</see>.
    ''' </summary>
    ''' <param name="observer">Specifies reference to a class implementing the 
    ''' <see cref="IEntityObserver">observer</see> contract</param>
    ''' <remarks></remarks>
    Protected Sub New(ByVal observer As IEntityObserver(Of TEntity))
        MyBase.New(observer)
    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)

        If Not MyBase.IsDisposed Then

            Try

                If disposing Then

                End If

                ' Free shared unmanaged resources

            Finally

                MyBase.Dispose(disposing)

            End Try

        End If

    End Sub

#End Region

#Region " ENTITY BASE METHOD - NO EVENTS "

    ''' <summary>
    ''' Fetches an entity or clears exiting entity.
    ''' </summary>
    ''' <param name="primaryKey1">Specifies the first primary key</param>
    ''' <param name="primaryKey2">Specifies the second primary key</param>
    ''' <returns>True if entity exists. Otherwise, a new entity is created.</returns>
    ''' <remarks></remarks>
    Protected MustOverride Function FetchEntity(ByVal primaryKey1 As TPrimaryKey1, ByVal primaryKey2 As TPrimaryKey2) As Boolean

#End Region

#Region " ENTITY "

    ''' <summary>
    ''' Fetches a new or exiting adapter using the primary key.
    ''' </summary>
    ''' <param name="primaryKey1">Specifies the first primary key</param>
    ''' <param name="primaryKey2">Specifies the second primary key</param>
    ''' <returns>True if entity exists. Otherwise, a new entity is created.</returns>
    ''' <remarks></remarks>
    Public Overridable Function Fetch(ByVal primaryKey1 As TPrimaryKey1, ByVal primaryKey2 As TPrimaryKey2) As Boolean Implements IEntityMultiSyncPublisher2(Of TEntity, TPrimaryKey1, TPrimaryKey2).Fetch

        Me.FetchEntity(primaryKey1, primaryKey2)
        OnEntityChanged()
        Return Not Me.Entity.IsNew

    End Function

    ''' <summary>
    ''' Finds an entity or clear exiting entity.
    ''' </summary>
    ''' <param name="primaryKey1">Specifies the first primary key</param>
    ''' <param name="primaryKey2">Specifies the second primary key</param>
    ''' <returns>True if entity exists.</returns>
    ''' <remarks>Find does not trigger the entity changed event</remarks>
    Public MustOverride Function Find(ByVal primaryKey1 As TPrimaryKey1, ByVal primaryKey2 As TPrimaryKey2) As Boolean Implements IEntityMultiSyncPublisher2(Of TEntity, TPrimaryKey1, TPrimaryKey2).Find

#End Region

End Class
