﻿''' <summary>
''' The contract implemented by classes implementing a repository.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="10/03/2010" by="David Hary" revision="1.0.3928.x">
''' Created
''' </history>
Public Interface IRepository(Of T)

    ''' <summary>
    ''' Gets the repository ID.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ReadOnly Property RepositoryId() As T

    ''' <summary>
    ''' Gets an adaptor to the local database.
    ''' </summary>
    ''' <param name="keepConnectionOpen"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function DataAccessAdaptorGetter(ByVal keepConnectionOpen As Boolean) As SD.LLBLGen.Pro.ORMSupportClasses.DataAccessAdapterBase

End Interface
