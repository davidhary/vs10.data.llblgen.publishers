﻿Imports isr.Core.EventHandlerExtensions
''' <summary>
''' Base class for anonymously publishing entity information.
''' This interfaces is independent of the entity type.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="10/03/2010" by="David Hary" revision="1.0.3928.x">
''' Created
''' </history>
Public MustInherit Class EntityPublisherBase

    Implements IEntityPublisher

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Constructs the class.
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub New()
        MyBase.New()
    End Sub

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make this method overridable (virtual) because a derived 
    '''   class should not be able to override this method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    Private _isDisposed As Boolean
    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derviced class
    ''' provided proper implementation.
    ''' </summary>
    Protected Property IsDisposed() As Boolean
        Get
            Return _isDisposed
        End Get
        Private Set(ByVal value As Boolean)
            _isDisposed = value
        End Set
    End Property

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        If Not Me.IsDisposed Then

            Try

                If disposing Then

                    If EntityChangedEvent IsNot Nothing Then
                        For Each d As [Delegate] In EntityChangedEvent.GetInvocationList
                            RemoveHandler EntityChanged, CType(d, EventHandler(Of Global.System.EventArgs))
                        Next
                    End If

                    If EntitySavedEvent IsNot Nothing Then
                        For Each d As [Delegate] In EntitySavedEvent.GetInvocationList
                            RemoveHandler EntitySaved, CType(d, EventHandler(Of Global.System.EventArgs))
                        Next
                    End If

                    If PropertyChangedEvent IsNot Nothing Then
                        For Each d As [Delegate] In PropertyChangedEvent.GetInvocationList
                            RemoveHandler PropertyChanged, CType(d, EventHandler(Of Global.System.ComponentModel.PropertyChangedEventArgs))
                        Next
                    End If

                    If MessageAvailableEvent IsNot Nothing Then
                        For Each d As [Delegate] In MessageAvailableEvent.GetInvocationList
                            RemoveHandler MessageAvailable, CType(d, EventHandler(Of isr.Core.MessageEventArgs))
                        Next
                    End If

                End If

                ' Free shared unmanaged resources

            Finally

                ' set the sentinel indicating that the class was disposed.
                Me.IsDisposed = True

            End Try

        End If

    End Sub

    ''' <summary>This destructor will run only if the Dispose method 
    '''   does not get called. It gives the base class the opportunity to 
    '''   finalize. Do not provide destructors in types derived from this class.</summary>
    Protected Overrides Sub Finalize()
        ' Do not re-create Dispose clean-up code here.
        ' Calling Dispose(false) is optimal in terms of
        ' readability and maintainability.
        Dispose(False)
        ' The compiler automatically adds a call to the base class finalizer 
        ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
        MyBase.Finalize()
    End Sub

#End Region

#Region " ENTITY NAME "

    ''' <summary>
    ''' Gets or sets the name of the entity.
    ''' </summary>
    ''' <value>
    ''' The name of the entity.
    ''' </value>
    Public MustOverride Property EntityName() As String Implements IEntityPublisher.EntityName

#End Region

#Region " ENTITY NO PUBLISH "

    ''' <summary>
    ''' Saves the active entity.
    ''' </summary>
    ''' <returns>True if entity was save and successfuly refetched.</returns>
    ''' <remarks>
    ''' Does not publish the <see cref="EntitySaved">entity saved</see> event.
    ''' </remarks>
    Public MustOverride Function SaveEntity() As Boolean Implements IEntityPublisher.SaveEntity

#End Region

#Region " ENTITY WITH PUBLISH "

    Private _AutoFetchEnabled As Boolean
    ''' <summary>
    ''' Enables fetching from the changed event. 
    ''' </summary>
    ''' <remarks></remarks>
    Public Overridable Property AutoFetchEnabled() As Boolean Implements IEntityPublisher.AutoFetchEnabled
        Get
            Return _AutoFetchEnabled
        End Get
        Set(ByVal value As Boolean)
            _AutoFetchEnabled = value
        End Set
    End Property

    ''' <summary>
    ''' Deletes the selected entity.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public MustOverride Function Delete() As Boolean Implements IEntityPublisher.Delete

    ''' <summary>
    ''' Instantiates a new entity.
    ''' </summary><returns></returns>
    Public MustOverride Function NewEntity() As Boolean Implements IEntityPublisher.NewEntity

#End Region

#Region " EVENTS "

    ''' <summary>
    ''' Notifies of changes in the entity.
    ''' </summary>
    ''' <remarks></remarks>
    Public Event EntityChanged As EventHandler(Of Global.System.EventArgs) Implements IEntityPublisher.EntityChanged

    ''' <summary>
    ''' Notifies that the entity was saved.
    ''' </summary>
    ''' <remarks></remarks>
    Public Event EntitySaved As EventHandler(Of Global.System.EventArgs) Implements IEntityPublisher.EntitySaved

    ''' <summary>
    ''' Updates the application entity.
    ''' </summary>
    ''' <remarks>
    ''' Disable auto fetch while updating the entity.
    ''' </remarks>
    ''' <history date="05/30/2011" by="David Hary" revision="2.1.4167.x">
    ''' fixes bug in restoring auto fetch
    ''' </history>
    Public Overridable Sub OnEntityChanged() Implements IEntityPublisher.OnEntityChanged
        Dim autoFetching As Boolean = Me.AutoFetchEnabled
        Try
            Me.AutoFetchEnabled = False
            EntityChangedEvent.SafeInvoke(Me)
        Catch
            Throw
        Finally
            Me.AutoFetchEnabled = autoFetching
        End Try
    End Sub

    ''' <summary>
    ''' Updates the application entity lists.
    ''' </summary>
    ''' <remarks></remarks>
    Protected Overridable Sub OnEntitySaved()
        EntitySavedEvent.SafeInvoke(Me)
    End Sub

    ''' <summary>
    ''' Handles the change in primary key.
    ''' </summary>
    ''' <remarks></remarks>
    Protected MustOverride Sub OnPrimaryKeyChanged()

    ''' <summary>
    ''' Notifies the observers of property changes.
    ''' </summary>
    ''' <remarks></remarks>
    Public Event PropertyChanged As EventHandler(Of Global.System.ComponentModel.PropertyChangedEventArgs) Implements IEntityPublisher.PropertyChanged

    ''' <summary>
    ''' Reports a property change.
    ''' </summary>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub OnPropertyChanged(ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        PropertyChangedEvent.SafeInvoke(Me, e)
    End Sub

    ''' <summary>
    ''' Refreshes this instance. Updates the entity after saving.
    ''' </summary>
    Public Sub Refresh() Implements IEntityPublisher.Refresh
        Me.OnEntitySaved()
        Me.OnEntityChanged()
    End Sub

    ''' <summary>
    ''' Saves the active entity.
    ''' </summary>
    ''' <returns>True if entity was save and successfuly refetched.</returns>
    ''' <remarks></remarks>
    Public MustOverride Function Save() As Boolean Implements IEntityPublisher.Save

#End Region

#Region " MESSAGE "

    ''' <summary>
    ''' Notifes of messages.
    ''' </summary>
    ''' <remarks></remarks>
    Public Event MessageAvailable As EventHandler(Of isr.Core.MessageEventArgs) Implements IEntityPublisher.MessageAvailable

    ''' <summary>
    ''' Displays and logs message.
    ''' </summary>
    ''' <param name="level">Messsage <see cref="TraceEventType">level</see></param>
    ''' <param name="synopsis">Short description of message</param>
    ''' <param name="format">A string format for formatting the message</param>
    ''' <param name="args">Paramater array for arguments</param>
    ''' <remarks></remarks>
    Protected Sub HandleMessage(ByVal level As TraceEventType, ByVal synopsis As String, ByVal format As String, ByVal ParamArray args() As Object)
        MessageAvailableEvent.SafeInvoke(Me, New isr.Core.MessageEventArgs(level, level, synopsis, Me.EntityName & ":: " & format, args))
    End Sub

#End Region

End Class
