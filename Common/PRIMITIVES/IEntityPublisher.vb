﻿''' <summary>
''' Defines the contract implemented by the entity anonymous publisher for an entity.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="10/03/2010" by="David Hary" revision="1.0.3928.x">
''' Created
''' </history>
Public Interface IEntityPublisher
    Inherits IDisposable

#Region " ENTITY NAME "

    ''' <summary>
    ''' Gets or sets the name of the entity.
    ''' </summary>
    ''' <value>
    ''' The name of the entity.
    ''' </value>
    Property EntityName() As String

#End Region

#Region " ENTITY NO PUBLISH "

    ''' <summary>
    ''' Saves the active entity.
    ''' </summary>
    ''' <returns>True if entity was save and successfuly refetched.</returns>
    ''' <remarks>
    ''' Does not publish the <see cref="EntitySaved">entity saved</see> event.
    ''' </remarks>
    Function SaveEntity() As Boolean

#End Region

#Region " ENTITY WITH PUBLISH "

    ''' <summary>
    ''' Enables fetching from the changed event. 
    ''' </summary>
    ''' <remarks></remarks>
    Property AutoFetchEnabled() As Boolean

    ''' <summary>
    ''' Deletes the selected entity.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function Delete() As Boolean

    ''' <summary>
    ''' Instantiates a new entity.
    ''' </summary><returns></returns>
    Function NewEntity() As Boolean

    ''' <summary>
    ''' Refreshes this instance. Updates teh entity after saving.
    ''' </summary>
    Sub Refresh()

    ''' <summary>
    ''' Saves the active entity.
    ''' </summary>
    ''' <returns>True if entity was save and successfuly refetched.</returns>
    ''' <remarks></remarks>
    Function Save() As Boolean

#End Region

#Region " EVENTS "

    ''' <summary>
    ''' Notifies of changes in the entity.
    ''' </summary>
    ''' <remarks></remarks>
    Event EntityChanged As EventHandler(Of Global.System.EventArgs)

    ''' <summary>
    ''' Updates the application entity. This effectively publishes the entity information.
    ''' </summary>
    ''' <remarks></remarks>
    Sub OnEntityChanged()

    ''' <summary>
    ''' Notifies that the entity was saved.
    ''' </summary>
    ''' <remarks></remarks>
    Event EntitySaved As EventHandler(Of Global.System.EventArgs)

    ''' <summary>
    ''' Notifes of messages.
    ''' </summary>
    ''' <remarks></remarks>
    Event MessageAvailable As EventHandler(Of isr.Core.MessageEventArgs)

    ''' <summary>
    ''' Notifies the observers of property changes.
    ''' </summary>
    ''' <remarks></remarks>
    Event PropertyChanged As EventHandler(Of Global.System.ComponentModel.PropertyChangedEventArgs)

#End Region

End Interface

